import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "./product";
import {ProductList} from "./product-list";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private backendURL = "http://localhost:8080/products"

  constructor( private http: HttpClient) { }

  public getProducts() : Observable<ProductList>{
    return this.http.get<ProductList>(this.backendURL)
  }

}
