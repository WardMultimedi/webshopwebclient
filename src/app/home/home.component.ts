import { Component, OnInit } from '@angular/core';
import {ProductService} from "../product.service";
import {Product} from "../product";

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  products: Product[];

  constructor( private productService:ProductService) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe((data) => {console.log(data); this.products = data.products});
  }

}
